Rails.application.routes.draw do

  root                              'home#index'

  get   'about'                 =>  'home#about'
  get   'faq'                   =>  'home#faq'
  get   'contact'               =>  'home#contact'

  resources :prerequisites
  resources :courses

end
