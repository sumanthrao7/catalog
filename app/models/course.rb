class Course < ActiveRecord::Base

  has_one :prerequisite, dependent: :destroy
  belongs_to :program
  
end
