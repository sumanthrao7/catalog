require 'rails_helper'

RSpec.describe "prerequisites/show", type: :view do
  before(:each) do
    @prerequisite = assign(:prerequisite, Prerequisite.create!(
      :operator => 1,
      :operandA => 2,
      :operandB => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
