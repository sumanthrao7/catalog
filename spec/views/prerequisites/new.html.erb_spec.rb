require 'rails_helper'

RSpec.describe "prerequisites/new", type: :view do
  before(:each) do
    assign(:prerequisite, Prerequisite.new(
      :operator => 1,
      :operandA => 1,
      :operandB => 1
    ))
  end

  it "renders new prerequisite form" do
    render

    assert_select "form[action=?][method=?]", prerequisites_path, "post" do

      assert_select "input#prerequisite_operator[name=?]", "prerequisite[operator]"

      assert_select "input#prerequisite_operandA[name=?]", "prerequisite[operandA]"

      assert_select "input#prerequisite_operandB[name=?]", "prerequisite[operandB]"
    end
  end
end
